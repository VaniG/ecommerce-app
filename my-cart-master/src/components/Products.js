import React from 'react';
import {ProductConsumer} from './Context';
import Product from './Product';

export default function Products() {
    return (
       <ProductConsumer>
    {function(value){
        const{data,closeNavCart, addToCart}= value;
        return(
    <div className="products">
      <h2>Products</h2>
      <div className="products-center">
          {data.map(data=>{
              return <Product key={data.id} data={data}  closeNavCart={closeNavCart} addToCart={()=>addToCart(data.id)}  />
          })}
        </div>
    </div>

    )
    }}
       </ProductConsumer>
    )
}

//import React from 'react';
//import axios from 'axios';
//
//class Example extends React.Component {
//
//   state = {
//       details: [],
//   }
//
//   componentDidMount() {
//
//       let data;
//
//       axios.get('http://127.0.0.1:8000/api/products/')
//           .then(res => {
//               data = res.data;
//               this.setState({
//                   details: data
//               });
//           })
//           .catch(err => {
//           })
//   }
//
//   render() {
//       return (
//           <div>
//               {this.state.details.map((detail, id) => (
//                       <div key={id}>
//                           <div>
//                               <div>
//
//                                   <h1>{detail.Title} </h1>
//                                   <h2>{detail.Description}</h2>
//                                   {detail.ImageLink}
//                                   <h3>{detail.Price}</h3>
//                                    <button onClick={(detail.id)}>Add to cart</button>
//
//
//                               </div>
//                           </div>
//                       </div>
//                   )
//               )}
//           </div>
//       );
//export default Example;