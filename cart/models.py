from django.db import models

# Create your models here.
from rest_framework_jwt.serializers import User


class Product(models.Model):
    Title = models.CharField(max_length=100)
    Description = models.CharField(max_length=200)
    Imagelink = models.CharField(max_length=500)
    Price = models.FloatField()
    CreatedAt = models.DateField(auto_now_add=True)
    UpdatedAt = models.DateField(auto_now=True)

    def __str__(self):
        return f"{self.id}{self.Title}{self.Description}{self.Imagelink}{self.Price}{self.CreatedAt}{self.UpdatedAt}"


class Order(models.Model):
    UserId = models.ForeignKey(User, on_delete=models.CASCADE)
    Total = models.IntegerField(default=0)
    CreatedAt = models.DateField(auto_now_add=True)
    UpdatedAt = models.DateField(auto_now=True)
    STATUS = (
        (1, 'New'),
        (2, 'Paid'),

    )
    # [...]
    Status = models.PositiveSmallIntegerField(
        choices=STATUS,
        default=1,
    )
    Mode_of_payment = (
        (1, 'Cash'),
        (2, 'Paytm'),
        (3, 'Card'),

    )
    # [...]
    Mode_of_payment = models.PositiveSmallIntegerField(
        choices=Mode_of_payment,
        default=1,
    )

    def __str__(self):
        return f"{self.id}{self.UserId}{self.Total}{self.CreatedAt}{self.UpdatedAt}{self.Status}{self.Mode_of_payment}"


class OrderItem(models.Model):
    Orderid = models.ForeignKey(Order, on_delete=models.CASCADE)
    Productid = models.ForeignKey(Product, on_delete=models.CASCADE)
    Quantity = models.IntegerField()
    Price = models.FloatField()

    def __str__(self):
        return f"{self.id}{self.Orderid}{self.Productid}{self.Quantity}{self.Price}"
