
from django.shortcuts import render

# Create your views here.
from requests import Response
from rest_framework import generics
from rest_framework.decorators import api_view

from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.pagination import PageNumberPagination


from cart.models import Product, Order, OrderItem
from cart.serializers import ProductSerializer, OrderSerializer, OrderItemSerializer
#
#
# class ProductPagination(PageNumberPagination):
#     page_size = 2




class List(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
#    pagination_class = ProductPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('id', 'Title')


class ProductDetails(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Product.objects.all()

    serializer_class = ProductSerializer


class OrderDetails(generics.ListCreateAPIView):
    def get_queryset(self):
        return Order.objects.all()

    serializer_class = OrderSerializer
 #   pagination_class = ProductPagination


class OrderUpdate(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Order.objects.all()

    serializer_class = OrderSerializer


class OrderItemDetails(generics.ListCreateAPIView):
    def get_queryset(self):
        return OrderItem.objects.all()

    serializer_class = OrderItemSerializer
  #  pagination_class = ProductPagination



class OrderItemUpdate(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return OrderItem.objects.all()

    serializer_class = OrderItemSerializer
